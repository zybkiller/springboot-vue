/*
 Navicat Premium Data Transfer

 Source Server         : zyb
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : database01

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 17/03/2022 10:34:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_sa_book
-- ----------------------------
DROP TABLE IF EXISTS `t_sa_book`;
CREATE TABLE `t_sa_book`  (
  `REC_ID` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '记录ID',
  `REC_DATE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `BOOK_NAME` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图书名称',
  `BOOK_PRICE` decimal(12, 2) NULL DEFAULT NULL COMMENT '图书价格',
  PRIMARY KEY (`REC_ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '图书测试表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_sa_book
-- ----------------------------
INSERT INTO `t_sa_book` VALUES ('99248607575670784', '20210511', 'Effective Java中文版', 94.00);
INSERT INTO `t_sa_book` VALUES ('99248607575670785', '20210511', 'Java编程思想', 75.00);
INSERT INTO `t_sa_book` VALUES ('99248607575670786', '20210511', 'Java核心技术', 98.30);
INSERT INTO `t_sa_book` VALUES ('99248607575670787', '20210511', '零基础学Java', 54.40);
INSERT INTO `t_sa_book` VALUES ('99248607575670788', '20210511', '深入理解Java虚拟机', 100.60);
INSERT INTO `t_sa_book` VALUES ('99248607575670789', '20210511', 'Java从入门到精通', 59.30);
INSERT INTO `t_sa_book` VALUES ('99248607575670790', '20210511', 'Head First Java中文版', 54.30);
INSERT INTO `t_sa_book` VALUES ('99248607575670791', '20210511', '数据结构与算法分析:Java语言描述', 50.60);
INSERT INTO `t_sa_book` VALUES ('99248607575670792', '20210511', 'Java项目开发实战入门', 46.60);
INSERT INTO `t_sa_book` VALUES ('99248607575670793', '20210511', 'C_SOFTWARE', 69.65);

SET FOREIGN_KEY_CHECKS = 1;
