package com.baosight.vue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages = "com.baosight.vue.mapper")
public class VueStarter {
    public static void main(String[] args) {
        SpringApplication.run(VueStarter.class, args);
    }
}
