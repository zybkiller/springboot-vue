package com.baosight.vue.model;

import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
@Builder
@Table(name = "T_SA_BOOK")
public class TBook implements Serializable {

    private String recId;

    private String recDate;

    private String bookName;

    private BigDecimal bookPrice;
}
