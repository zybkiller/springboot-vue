package com.baosight.vue.mapper;


import com.baosight.vue.model.TBook;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;
import java.util.Map;

public interface TBookMapper extends Mapper<TBook> {

    int deleteByRecId(String recId);

    int insert1(TBook record);

    TBook selectByRecId(String recId);

    @Override
    List<TBook> selectAll();

    int updateByRecId(TBook record);

    List<TBook> queryByRecDate(String recDate);

    int batchAddBooks(List<TBook> list);

    int batchAlterBooks(List<Map<String, Object>> list);

    int batchDelBooks(List<Map<String, Object>> list);
}
