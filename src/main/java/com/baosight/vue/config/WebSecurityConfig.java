package com.baosight.vue.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .mvcMatchers("/webapp/vue_demo1.html").permitAll()//放行资源写在任何前面
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .successForwardUrl("/show")//认证成功 forward 跳转路径  始终在认证成功之后跳转到指定请求
                .and()
                .csrf().disable();
    }

}
