package com.baosight.vue.controller;

import com.baosight.vue.model.TBook;
import com.baosight.vue.service.TBookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "图书信息接口")
@Slf4j
public class TBookController {

    @Autowired
    private TBookService tBookService;

    @PostMapping("/query1")
    @ApiOperation(value = "queryAll")
    public List<TBook> query1(@RequestBody Map map) {
        log.info("执行查询...");
        String recDate = map.get("recDate").toString();
        return tBookService.query1(recDate);
    }

    @PostMapping("/add1")
    @ApiOperation(value = "add1")
    public int add1(@RequestBody TBook tBook) {
        log.info("执行新增...");
        return tBookService.add1(tBook);
    }

    @PostMapping("/update1")
    @ApiOperation(value = "update1")
    public int updateByRecId(@RequestBody TBook tBook) {
        log.info("执行修改...");
        return tBookService.updateByRecId(tBook);
    }

    @PostMapping("/delete1")
    @ApiOperation(value = "delete1")
    public int deleteByRecId(@RequestBody Map<String, Object> map) {
        log.info("执行删除...");
        String recId = map.get("recId").toString();
        return tBookService.deleteByRecId(recId);
    }

    @PostMapping("/batchAdd")
    @ApiOperation(value = "batchAdd")
    public int batchAddBooks(@RequestBody List<TBook> list) {
        log.info("批量新增...");
        return tBookService.batchAddBooks(list);
    }

    @PostMapping("/batchAlter")
    @ApiOperation(value = "batchAlter")
    public int batchAlterBooks(@RequestBody List<Map<String, Object>> list) {
        log.info("批量修改...");
        return tBookService.batchAlterBooks(list);
    }

    @PostMapping("/batchDel")
    @ApiOperation(value = "batchDel")
    public int batchDelBooks(@RequestBody List<Map<String, Object>> list) {
        log.info("批量删除...");
        return tBookService.batchDelBooks(list);
    }
}
