package com.baosight.vue.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JumpController {

    /**
     * @author zyb
     * @description forward request path
     * @datetime 2022/3/17 10:31
     * @params []
     * @returns java.lang.String
     **/
    @RequestMapping(path = "/show")
    public String turn2vueDemo() {
        return "vue_demo1";
    }

}
