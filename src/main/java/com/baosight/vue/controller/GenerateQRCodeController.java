package com.baosight.vue.controller;

import com.baosight.vue.utils.QRCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author ZYB
 * @date 2022/6/15
 */
@RestController
@RequestMapping(path = "/QRCode")
@Slf4j
public class GenerateQRCodeController {

    /**
     * @author zyb
     * @description 生成二维码
     * @datetime 2022/6/15 9:36
     * @params [response]
     * @returns void
     **/
    @GetMapping(value = "/generateQRCode")
    public void getQrCodeImage(HttpServletResponse response) {
        try (OutputStream os = response.getOutputStream()) {
            String url = "https://zybccc.com";            // 生成二维码对象
            BufferedImage image = QRCodeUtils.getQrLogoCode(url, null);            //设置response
            response.setContentType("image/png");            // 输出jpg格式图片
            ImageIO.write(image, "jpg", os);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("二维码生成失败!");
        }
    }

    /**
     * @author zyb
     * @description 生成带logo的二维码
     * @datetime 2022/6/15 9:36
     * @params [response]
     * @returns void
     **/
    @GetMapping(value = "/generateQRCodeWithLogo")
    public void getQrLogoCodeImage(HttpServletResponse response) {
        try (OutputStream os = response.getOutputStream()) {
            String url = "https://zybccc.com";
            String logoPath = "https://zybccc.com/images/my_logo.jpg";            // 生成二维码对象
            BufferedImage image = QRCodeUtils.getQrLogoCode(url, logoPath);            //设置response
            response.setContentType("image/png");            // 输出jpg格式图片
            ImageIO.write(image, "jpg", os);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("二维码生成失败!");
        }
    }

    /**
     * @author zyb
     * @description 解析二维码图片，返回字符串
     * @datetime 2022/6/15 9:36
     * @params [file]
     * @returns java.lang.String
     **/
    @PostMapping(value = "/parsingQRCode")
    public String decodeQrImage(@RequestParam("file") MultipartFile file) {
        try {
            InputStream inputStream = file.getInputStream();
            return QRCodeUtils.decodeQrImage(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("二维码解析失败!");
        }
    }
}
