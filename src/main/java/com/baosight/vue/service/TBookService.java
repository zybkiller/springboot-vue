package com.baosight.vue.service;

import com.baosight.vue.mapper.TBookMapper;
import com.baosight.vue.model.TBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
public class TBookService {

    @Autowired
    private TBookMapper tBookMapper;

    public List<TBook> query1(String recDate) {
        return tBookMapper.queryByRecDate(recDate);
    }

    public int add1(TBook tBook) {
        return tBookMapper.insert(tBook);
    }

    public int updateByRecId(TBook record) {
        return tBookMapper.updateByRecId(record);
    }

    public int deleteByRecId(String recId) {
        return tBookMapper.deleteByRecId(recId);
    }

    public int batchAddBooks(List<TBook> list) {
        return tBookMapper.batchAddBooks(list);
    }

    public int batchAlterBooks(List<Map<String, Object>> list) {
        return tBookMapper.batchAlterBooks(list);
    }

    public int batchDelBooks(List<Map<String, Object>> list) {
        return tBookMapper.batchDelBooks(list);
    }
}
